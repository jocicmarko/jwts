package jwts.wafepa.web.controller;

import javax.validation.Valid;

import jwts.wafepa.model.Activity;
import jwts.wafepa.service.ActivityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/activities")
public class ActivityController {

	@Autowired
	private ActivityService activityService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String get(Model model) {
		model.addAttribute("activities", activityService.findAll());
		return "activities";
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String getNew(Model model) {
		model.addAttribute("activity", new Activity());
		return "addEditActivity";
	}	
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String getEdit(@PathVariable Long id, Model model) {
		model.addAttribute("activity", activityService.findOne(id));
		return "addEditActivity";
	}	
	
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
	public String remove(@PathVariable Long id) {
		activityService.remove(id);
		return "redirect:/activities";
	}	
	
	@RequestMapping(method = RequestMethod.POST)
	public String post(@Valid Activity activity, BindingResult bindingResult, Model model) {
		String viewName;
		if (!bindingResult.hasErrors()) {
			activityService.save(activity);
			viewName = "redirect:activities";
		} else {
			model.addAttribute("activity", activity);
			viewName = "addEditActivity";
		}
		
		return viewName;
	}	
}
