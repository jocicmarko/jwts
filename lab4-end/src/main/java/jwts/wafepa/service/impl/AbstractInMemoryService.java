package jwts.wafepa.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import jwts.wafepa.model.AbstractBaseEntity;
import jwts.wafepa.service.CrudService;

public abstract class AbstractInMemoryService<T extends AbstractBaseEntity> implements CrudService<T> {

	protected static final String NON_EXISTING_ENTITY = "Error: Tried to remove non-existing entity with id=%d.";
	
	private final Map<Long, T> map = new HashMap<>();
	protected final AtomicLong sequence = new AtomicLong(1);

	@Override
	public T findOne(Long id) {
		return map.get(id);
	}

	@Override
	public List<T> findAll() {
		return new ArrayList<>(map.values());
	}

	@Override
	public T save(T t) {
		if (t.getId() == null) {
			t.setId(sequence.getAndIncrement());
		}
		map.put(t.getId(), t);
		return t;
	}

	@Override
	public void remove(Long id) throws IllegalArgumentException {
		T t = map.remove(id);
		if (t == null) {
			throw new IllegalArgumentException(String.format(NON_EXISTING_ENTITY, id));
		}
	}
}
