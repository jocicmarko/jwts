<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title>Add/Edit User</title>
</head>
<body>
	<div id="addEditUser">
		<c:url var="users" value="/users" />
		<a href="${users}">Back to users</a>
		<form:form id="formUser" action="${users}" method="post" modelAttribute="user">
			<fieldset>
				<form:hidden path="id" />
				
				<form:label path="email">Email </form:label>
				<form:input path="email" />
				
				<form:label path="firstname">First name </form:label>
				<form:input path="firstname" />
				
				<form:label path="lastname">Last name </form:label>
				<form:input path="lastname" />
			</fieldset>
			<p><button type="submit" >Submit</button></p>
		</form:form>
	</div>	
</body>
</html>