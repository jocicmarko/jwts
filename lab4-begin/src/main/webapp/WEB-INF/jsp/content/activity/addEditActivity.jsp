<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<h2>Add/Edit Activity</h2>
<div id="addEditActivity">
	<c:url var="activities" value="/activities" />
	<a href="${activities}">Back to activities</a>
	<form:form id="formActivity" action="${activities}" method="post" modelAttribute="activity">
		<fieldset>
			<form:hidden path="id" />
			<form:label path="name">Name </form:label>
			<form:input path="name" />
		</fieldset>
		<div class="highlightcolorBlack">
			<button type="submit" name="save" class="button">Submit</button>
			<button type="reset" name="cancel" class="button">Cancel</button>
		</div>
	</form:form>
</div>