<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<h2>Activities</h2>
<div id="activities">
	<table class="dataTable">
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th></th>
		</tr>
		<c:forEach items="${activities}" var="activity">
			<tr>	
				<td><c:out value="${activity.id}"/></td>				
				<td><c:out value="${activity.name}"/></td>
				<td><a href="<c:url value="/activities/edit/${activity.id}"/>">Edit</a></td>
				<td><a href="<c:url value="/activities/remove/${activity.id}"/>">Remove</a></td>
			</tr>
		</c:forEach>
	</table>
</div>	
<div id="newActivity" class="highlightcolorBlack">
	<a href="<c:url value="/activities/new"/>">Create new activity</a>
</div>
