<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<h2>Users</h2>
<div id="users">
	<table class="dataTable">
		<tr>
			<th>ID</th>
			<th>Email</th>
			<th>First name</th>
			<th>Last name</th>
			<th></th>
		</tr>
		<c:forEach items="${users}" var="user">
			<tr>	
				<td><c:out value="${user.id}"/></td>				
				<td><c:out value="${user.email}"/></td>
				<td><c:out value="${user.firstname}"/></td>
				<td><c:out value="${user.lastname}"/></td>
				<td><a href="<c:url value="/users/edit/${user.id}"/>">Edit</a></td>
				<td><a href="<c:url value="/users/remove/${user.id}"/>">Remove</a></td>
			</tr>
		</c:forEach>
	</table>
</div>	
<div id="newUser" class="highlightcolorBlack">
	<a href="<c:url value="/users/new"/>">Create new user</a>
</div>
