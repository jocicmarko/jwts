<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<h2>Add/Edit User</h2>
<div id="addEditUser">
	<c:url var="users" value="/users" />
	<a href="${users}">Back to users</a>
	<form:form id="formUser" action="${users}" method="post" modelAttribute="user">
		<fieldset>
			<form:hidden path="id" />
			<form:label path="email">Email </form:label>
			<form:input path="email" />
			<br />
			
			<form:label path="firstname">First name </form:label>
			<form:input path="firstname" />
			<br />
			
			<form:label path="lastname">Last name </form:label>
			<form:input path="lastname" />
		</fieldset>
		<div class="highlightcolorBlack">
			<button type="submit" name="save" class="button">Submit</button>
			<button type="reset" name="cancel" class="button">Cancel</button>
		</div>
	</form:form>
</div>	