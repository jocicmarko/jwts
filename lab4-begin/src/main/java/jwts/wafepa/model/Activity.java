package jwts.wafepa.model;

public class Activity extends AbstractBaseEntity {

	private static final long serialVersionUID = -4614577547027982134L;
	
	private String name;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
