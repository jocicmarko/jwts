## Lab 4 - Validacija formi, i18n

----

### Validacija formi

U Spring-u, [validacija podataka](http://docs.spring.io/spring/docs/4.0.2.RELEASE/spring-framework-reference/htmlsingle/#validation-beanvalidation) se vrši anotiranjem klasa i određenih polja klasa.
Spring Framework nudi velik skup predefinsanih validatora sa njima odgovarajućim porukama greške.
Validacija formi je direktno podržana kroz biblioteku form tagova.

----

* Dodati potrebne dependency-je za validaciju

```xml
<!-- Validation -->
<dependency>
	<groupId>javax.validation</groupId>
	<artifactId>validation-api</artifactId>
	<version>1.1.0.Final</version>
</dependency>
<dependency>
	<groupId>org.hibernate</groupId>
	<artifactId>hibernate-validator</artifactId>
	<version>5.1.0.Final</version>
</dependency>
```

* Anotirati polje name u klasi Activity sa @NotNull i @Length(min=1, max=30)

* Izmeniti post metodu ActivityController-a tako da je Activity parametar anotiran sa @Valid.
Takođe, kao parametre dodati BindingResult i Model. Parametar BindingResult omogućava proveru da li su podaci anotirani sa @Valid validni.
Parametar Model omogućava prosleđivanje poruke o grešci nazad na view.

```java
@RequestMapping(method = RequestMethod.POST)
public String post(@Valid Activity activity, BindingResult bindingResult, Model model) {
...
}
```

* Izmeniti view za dodavanje/izmenu aktivnosti tako da koristi tagove za poruke greške (form:errors)

* Dodati stilove za poruke greške

```xml
<form:input path="name" cssErrorClass="error" />
<form:errors path="name" cssClass="errorMessage" />
```

----

### I18N

I18N (internationalization) predstavlja dizajniranje i razvoj proizvoda,
aplikacije ili bilo kakvog sadržaja tako da bude obezbeđena laka lokalizacija
za različite ciljne grupe koje variraju u kulturi, državi ili jeziku.

Spring Framework ima direktnu podršku za i18n, što omogućava razvoj web aplikacije
tako da lako bude dostupna na svim podržanim jezicima.

----

* Napraviti direktorijume cfg i cfg/messsages u src/main/resources.
Unutar cfg/messages direktorijuma napraviti fajlove messages.properties i messages_sr.properties.
U ovim fajlovima će biti svi potrebni lokalizovani tekstovi i poruke koje će se prikazivati korisnicima.

* Dodati i18n konfiguraciju u mvc-config.xml.
Bean messageSource definiše putanju na kojoj se nalaze fajlovi sa lokalizovanim sadržajem.
Spring zna da se nastavak _sr odnosi na srpski jezik.
Bean localeResolver u sesiji čuva koji je trenutno aktiviran lokal (jezik) na web aplikaciji.
Poslednji deo konfiguracije predstavlja definisanje parametra (lang) kroz koji se može direktno postaviti željeni lokal.

```xml
<!-- Messages (i18n) -->
<bean id="messageSource" class="org.springframework.context.support.ResourceBundleMessageSource"
	p:basename="cfg.messages.messages" />

<bean id="localeResolver" class="org.springframework.web.servlet.i18n.SessionLocaleResolver" />

<mvc:interceptors>
	<bean class="org.springframework.web.servlet.i18n.LocaleChangeInterceptor" p:paramName="lang" />
</mvc:interceptors>
```

* U messages.properties dodati sledeću liniju:

```java
page.activities.header=Activities
```

* U messages_sr.properties dodati sledeću liniju:

```java
page.activities.header=Aktivnosti
```

* U activites.jsp izmeniti H2 element da umesto teksta sadrži sledeće:

```xml
<fmt:message key="page.activities.header"/>
```

Ovim je omogućeno da Spring pročita određeni tekst definisan ključem (key="page.activities.header")
iz messages fajlova.

* U standardLayout.jsp dodati u header elementu sledeći deo koda koji
će omogućiti promenu lokala:

```xml
<a href="<c:url value="?lang=sr" />">SR</a>
<a href="<c:url value="?lang=en" />">EN</a>
```

* Po uzoru na prethodni primer, zameniti sve tekstove u JSP stranicama
sa lokalizovanim tesktovima iz messages fajlova (prvo dodati lokalizovane
tekstove u messages i messages_sr fajlove i voditi računa da ključevi budu jedinstveni).

* Testirati aplikaciju uz promenu lokala.

#### Lokalizovane poruke greške
Spring omogućava definisanje lokalizvanih poruka koji će se
ispisivati kao poruke greške pri validaciji. Neophodno je u messages fajlovima
dodati lokalizovane poruke, ali tako da ključ odgovara anotaciji koja je korišćena za validaciju polja
(npr. ako je korišćenja anotacija NotNull, to mora biti ključ za odgovarajuću poruku u messages fajlu).

* U messages.properties dodati sledeću liniju:

```java
Length=Field must of length between 1 and 30.
```

* U messages_sr.properties dodati sledeću liniju:

```java
Length=Polje mora biti dužine između 1 i 30.
```

* Testirati unos podataka na formi uz promenu lokala.
----

### Domaći zadatak:
1. Dodati validacije za klasu User
	- @NotEmpty i @Email za email
	- @NotNull i @Length za firstname i lastname
2. Dodati podršku za i18n na stranicama za evidenciju korisnika
