## Java Web Training Seminar

### Dobrodošli na JWTS!

Kurs se sastoji iz sledećih radionica:
* Lab 1 - Uvod u Maven i Spring Framework
* Lab 2 - Spring MVC, Dependency injection
* Lab 3 - Apstrahovanje servisnog sloja, Apache Tiles, CSS i JS statički resursi
* Lab 4 - Validacija formi, i18n
* Lab 5 - Data binding, automatsko testiranje kontrolera
* Lab 6 - Spring Data JPA, Hibernate
* Lab 7 - Spring Data JPA, Hibernate, razdvajanje projekta na module
* Lab 8 - Bootstrap (CSS i JavaScript)
* Lab 9 - RESTful web servisi, JSON, SPA, Backbone.js
* Lab 10 - SPA, Backbone.js
