<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title>Activities</title>
</head>
<body>
	<div id="activities">
		<table>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th></th>
			</tr>
			<c:forEach items="${activities}"  var="activity">
				<tr>	
					<td><c:out value="${activity.id}"/></td>				
					<td><c:out value="${activity.name}"/></td>
					<td><a href="activities/edit/${activity.id}">Edit</a></td>
					<td><a href="activities/remove/${activity.id}">Remove</a></td>
				</tr>
			</c:forEach>
		</table>
	</div>	
	<div id="newActivity">
		<a href="activities/new">Create new activity</a>
	</div>
</body>
</html>