## Lab 5 - Automatsko testiranje kontrolera

----

### Proširivanje modela aplikacije

* Dodati model klasu ActivityLog. Ova klasa predstavlja zapis o fizičkoj
aktivnosti jednog korisnika. Polja klase:
	- Long id
	- Activity activity - koja fizička aktivnost je u pitanju
	- User user - koji korisnik je radio fizičku aktivnost
	- Integer duration - koliko minuta je trajala ova fizička aktivnost
	- Double length - koliko metara je pređeno tokom ove fizičke aktivnosti (ako je u pitanju trčanje, plivanje, itd.)
	- Date date - datum aktivnosti

* Dodati servisni sloj za ActivityLog

### Automatsko testiranje kontrolera
Automatsko testiranje kontrolera omogućava verifikaciju funkcionalnosti kontrolera
bez potrebe pravljenja view-ova. Ovo je velika prednost u razvoju softvera u timu kada jedan
deo tima razvija view, a drugi deo tima razvija poslovnu logiku - ovim je omogućeno testiranje
kontrolera dok se view još uvek razvija i dok nije spreman za testiranje.

* Napraviti paket jwts.wafepa.web.controller src/test/java i dodati klasu ActivityControllerTest

* Konfigurisati kontekst za izvršavanje testova pomoću anotacija:

```java
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:ctx/application-config.xml")
public class ActivityControllerTest {

	@Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
	
}
```

* Inicijalizovati podatke za testiranje

* Importovati bildere HTTP zahteva i rezultata:

```java
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
```

* Napisati testove za metode kontrolera, na primer:

```java
@Test
public void getActivities() throws Exception {
	MvcResult result = this.mockMvc.perform(get("/activities"))
		.andExpect(status().isOk())
		.andReturn();
	
	ModelAndView mav = result.getModelAndView();
	Assert.assertNotNull(mav.getModel().get("activities"));
	List<Activity> activities = (List<Activity>) mav.getModel().get("activities");
	Assert.assertEquals(2, activities.size());
}
```