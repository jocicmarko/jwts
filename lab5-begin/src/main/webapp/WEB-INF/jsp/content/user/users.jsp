<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<h2><fmt:message key="page.users.header"/></h2>
<div id="users">
	<table class="dataTable">
		<tr>
			<th><fmt:message key="common.label.id"/></th>
			<th><fmt:message key="common.label.email"/></th>
			<th><fmt:message key="common.label.firstname"/></th>
			<th><fmt:message key="common.label.lastname"/></th>
			<th></th>
		</tr>
		<c:forEach items="${users}" var="user">
			<tr>	
				<td><c:out value="${user.id}"/></td>				
				<td><c:out value="${user.email}"/></td>
				<td><c:out value="${user.firstname}"/></td>
				<td><c:out value="${user.lastname}"/></td>
				<td><a href="<c:url value="/users/edit/${user.id}"/>"><fmt:message key="common.action.edit"/></a></td>
				<td><a href="<c:url value="/users/remove/${user.id}"/>"><fmt:message key="common.action.remove"/></a></td>
			</tr>
		</c:forEach>
	</table>
</div>	
<div id="newUser" class="highlightcolorBlack">
	<a href="<c:url value="/users/new"/>"><fmt:message key="page.users.action.addNewUser"/></a>
</div>
