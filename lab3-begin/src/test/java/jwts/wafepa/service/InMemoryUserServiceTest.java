package jwts.wafepa.service;

import java.util.List;

import jwts.wafepa.model.User;
import jwts.wafepa.service.impl.InMemoryUserService;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InMemoryUserServiceTest {

	private UserService userService;
	
	@Before
	public void setUp() {
		userService = new InMemoryUserService();
		
		User user1 = new User();
		user1.setId(1L);
		user1.setEmail("email1@wafepa.com");
		user1.setFirstname("John");
		user1.setLastname("Doe");
		
		User user2 = new User();
		user2.setId(2L);
		user2.setEmail("email2@wafepa.com");
		user2.setFirstname("Jayna");
		user2.setLastname("Doe");
		
		userService.save(user1);
		userService.save(user2);
	}
	
	@Test
	public void testFindOne() {
		User user = userService.findOne(1L);
		Assert.assertNotNull(user);
		Assert.assertEquals("email1@wafepa.com", user.getEmail());
		Assert.assertEquals("John", user.getFirstname());
		Assert.assertEquals("Doe", user.getLastname());
	}
	
	@Test
	public void testFindAll() {
		List<User> users = userService.findAll();
		Assert.assertEquals(2, users.size());
		User user1 = users.get(0);
		User user2 = users.get(1);
		if (user1.getId().equals(1L)) {
			Assert.assertEquals("email1@wafepa.com", user1.getEmail());
			Assert.assertTrue(user2.getId().equals(2L));
		} else {
			Assert.assertTrue(user1.getId().equals(2L));
			Assert.assertTrue(user2.getId().equals(1L));
		}
	}

	@Test
	public void testSave() {
		User user = new User();
		user.setEmail("email3@wafepa.com");
		user.setFirstname("New");
		user.setLastname("User");
		User saved = userService.save(user);
		
		Assert.assertNotNull(saved.getId());		
		
		user = userService.findOne(saved.getId());
		Assert.assertEquals("email3@wafepa.com", user.getEmail());
		Assert.assertEquals("New", user.getFirstname());
		Assert.assertEquals("User", user.getLastname());
	}

	@Test
	public void testRemove() {
		Assert.assertNotNull(userService.findOne(1L));
		Assert.assertNotNull(userService.findOne(2L));
		
		userService.remove(1L);
		
		Assert.assertNull(userService.findOne(1L));
		Assert.assertNotNull(userService.findOne(2L));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testRemoveIllegalArgument() {
		Assert.assertNull(userService.findOne(3L));		
		userService.remove(3L);
	}
	
	@After
	public void tearDown() {
		// free resources
	}
}
