<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<title>Activities</title>
</head>
<body>
	<div id="users">
		<table>
			<tr>
				<th>ID</th>
				<th>Email</th>
				<th>First name</th>
				<th>Last name</th>
				<th>Actions</th>
			</tr>
			<c:forEach items="${users}"  var="user">
				<tr>	
					<td><c:out value="${user.id}"/></td>				
					<td><c:out value="${user.email}"/></td>
					<td><c:out value="${user.firstname}"/></td>
					<td><c:out value="${user.lastname}"/></td>
					<td><a href="<c:url value="/users/edit/${user.id}"/>">Edit</a></td>
					<td><a href="<c:url value="/users/remove/${user.id}"/>">Remove</a></td>
				</tr>
			</c:forEach>
		</table>
	</div>	
	<div id="newUser">
		<a href="<c:url value="/users/new"/>">Create new user</a>
	</div>
</body>
</html>