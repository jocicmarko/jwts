## Lab 3 - Apstrahovanje servisnog sloja, Apache Tiles, CSS i JS statički resursi

### Apstrahovanje servisnog sloja

Kada se doda nova model klasa, potrebno je za nju napraviti i servisni sloj.
Potrebno je primetiti da bi kod za ovaj novi servis izgledao 95% isto kao i za bilo koji drugi, razlika bi bila jedino u korišćenom entitetu, tj na koji model se odnosi.
Ovo se može rešiti apstrahovanjem servisnog sloja:

* Napraviti šablon interfejs CrudService<T> sa CRUD metodama. Postojeće servisne interfejse izmeniti tako da nasleđuju CrudService

* Napraviti apstraktnu model klasu AbstractBaseEntity. Ova klasa će služiti kao osnov za sve ostale model klase koje imaju polje ID.
Napraviti da AbstractBaseEntity nasleđuje Serializable, i generisati serialUID pomoću Eclipse-a

* Napraviti šablonsku apstraktnu klasu AbstractInMemoryService<T> koja implementira interfejs CrudService.
Postojeće in-memory implementacije servisa izmeniti da nasleđuju AbstractInMemoryService i imlpementiraju njima odgovarajući servisni interfejs

### Razdvajanje konfiguracije aplikacije (application-config.xml) na bean-config.xml i mvc-config.xml.
bean-config.xml treba da sadrži konfiguraciju koja se tiče poslovne logike i nije direktno vezana za web tehnologije.
mvc-config.xml treba da sadrži konfiguraciju koja se odnosi web.
Pobrisati nepotrebne XML namespace-ove i u application-config.xml importovati napravljene konfiguracione fajlove.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-4.0.xsd">

	<!-- Core configuration -->
	<import resource="classpath:/ctx/bean-config.xml"/>

	<!-- Spring MVC -->
	<import resource="classpath:/ctx/mvc-config.xml"/>

</beans>
```

### Apache Tiles

#### Standard layout

![LAYOUT](https://dl.dropboxusercontent.com/u/11650317/layout.png)

1. [Composite View pattern](https://tiles.apache.org/framework/tutorial/pattern.html) i [View Helper pattern](http://www.oracle.com/technetwork/java/viewhelper-139885.html)

2. [Apache Tiles](http://tiles.apache.org/)

------------

* Dodati Apache Tiles dependency-je u pom.xml

```xml
<!-- Tiles -->
<dependency>
	<groupId>org.apache.tiles</groupId>
	<artifactId>tiles-api</artifactId>
	<version>3.0.3</version>
</dependency>
<dependency>
	<groupId>org.apache.tiles</groupId>
	<artifactId>tiles-core</artifactId>
	<version>3.0.3</version>
</dependency>
<dependency>
	<groupId>org.apache.tiles</groupId>
	<artifactId>tiles-jsp</artifactId>
	<version>3.0.3</version>
</dependency>
```

* Unutar WEB-INF direktorijuma napraviti novi direktorijum tiles-def i u njemu napraviti XML fajl tiles.xml.
U tiles.xml ubaciti sledeći deo koda:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE tiles-definitions PUBLIC "-//Apache Software Foundation//DTD Tiles Configuration 3.0//EN" "http://tiles.apache.org/dtds/tiles-config_3_0.dtd">
<tiles-definitions>
	
</tiles-definitions>
```

* Izmeniti mvc-config.xml da koristi Tiles view resolver i dodati Tiles konfiguraciju:

```xml
<bean id="tilesViewResolver" class="org.springframework.web.servlet.view.tiles3.TilesViewResolver" />

<bean id="tilesConfigurer" class="org.springframework.web.servlet.view.tiles3.TilesConfigurer">
	<property name="definitions">
		<list>
			<value>/WEB-INF/tiles-def/tiles.xml</value>
		</list>
	</property>
</bean>
```

* U tiles.xml dodati definiciju standardnog layout-a:

```xml
<definition name="standardLayout" template="/WEB-INF/jsp/layout/standardLayout.jsp">
	<put-attribute name="title" value="" />
	<put-attribute name="content" value="" />
</definition>
```

* U WEB-INF/jsp direktorijumu napraviti tri nova direktorijuma: common, layout i content.
U common direktorijumu napraviti JSP stranicu tagLibs.jsp.
Ova JSP stranica treba da sadrži potrebne importe za razne JSP i JSTL tagove:

```xml
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
```

* U napravljenom WEB-INF/jsp/layout direktorijumu napraviti JSP stranicu standardLayout.jsp.
Obratiti pažnju na elemente <tiles:importAttribute name="title" /> i <tiles:insertAttribute name="content" />.

```xml
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<tiles:importAttribute name="title" />

<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>${title}</title>
		
	</head>
	<body>
		<header>
			<div>
				<a href="#" target="_blank">Faculty of Technical Sciences and Levi9 IT Services</a>
			</div>
			<h1>WAFEPA</h1>
			<ul>
				<li><a href="<c:url value="/"/>">Home</a></li>
				<li><a href="<c:url value="/activities"/>">Activities</a></li>
				<li><a href="<c:url value="/users"/>">Users</a></li>
			</ul>
		</header>
		<section id="mainContent">
			<tiles:insertAttribute name="content" />
		</section>
		<footer>
			<div>&copy; 2014. Faculty of Technical Sciences and Levi9 IT Services</div>
		</footer>
	</body>
</html>
```

* Refaktorisati postojeće stranice za CRUD operacije nad aktivnostima da nasleđuju standardLayout.jsp i dodati potrebne view-ove u tiles.xml


----

### CSS i JS statički resursi

* Napraviti css i js direktorijume u WEB-INF

* Dodati mapiranje za statičke resurse u mvc-config.xml - [Configure static resource resolver](http://docs.spring.io/spring/docs/4.0.2.RELEASE/spring-framework-reference/htmlsingle/#mvc-config-static-resources)

```xml
<mvc:resources location="/css/" mapping="/css/**" />
<mvc:resources location="/js/" mapping="/js/**" />
```

* Napraviti JavaScript fajl main.js u WEB-INF/js - ovde će se pisati klijentski JavaScript/jQuery kod

* Napraviti WEB-INF/js/lib/jquery direktorijum i dodati jquery-1.10.2.min.js

* Iskoristiti jQuery za postavljanje inicijalnog fokusa na text input element na svakoj stranici

* Dodati screen.css u WEB-INF/css direktorijum

* Uključiti sve potrebne CSS i JS fajlove u stranicama

* Dodati favicon http://www.levi9.com/wp-content/themes/levi9/favicon.ico

* Dodeliti odgovarajuće klase i stilove HTML elementima

----

### Domaći zadatak:
1. Po uzoru na stranice za evidenciju aktivnosti (klasa Activity), refaktorisati stranice za evidenciju korisnika (klasa User).
Takođe dodati potrebne view-ove u tiles.xml.