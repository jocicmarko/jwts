package jwts.wafepa.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;

import jwts.wafepa.model.Activity;
import jwts.wafepa.service.ActivityService;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:ctx/application-config.xml")
public class ActivityControllerTest {

	@Autowired
    private WebApplicationContext wac;
	@Autowired
	private ActivityService activityService;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        
        Activity activityRunning = new Activity();
		activityRunning.setId(1L);
		activityRunning.setName("Running");
		
		Activity activitySwimming = new Activity();
		activitySwimming.setId(2L);
		activitySwimming.setName("Swimming");
		
		activityService.save(activityRunning);
		activityService.save(activitySwimming);
    }
    
    @After
    public void tearDown() {
    	List<Activity> activities = activityService.findAll();
    	for (Activity activity : activities) {
    		activityService.remove(activity.getId());
    	}
    }

    @Test
    public void getActivities() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/activities"))
            .andExpect(status().isOk())
            .andReturn();
        
        ModelAndView mav = result.getModelAndView();
        Assert.assertNotNull(mav.getModel().get("activities"));
        List<Activity> activities = (List<Activity>) mav.getModel().get("activities");
        Assert.assertEquals(2, activities.size());
    }
    
    @Test
    public void getNewActivity() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/activities/new"))
            .andExpect(status().isOk())
            .andReturn();
        
        ModelAndView mav = result.getModelAndView();
        Assert.assertNotNull(mav.getModel().get("activity"));
        Activity activity = (Activity) mav.getModel().get("activity");
        Assert.assertNull(activity.getId());
        Assert.assertNull(activity.getName());
    }
    
    @Test
    public void getEditActivity() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/activities/edit/{id}", 1))
            .andExpect(status().isOk())
            .andReturn();
        
        ModelAndView mav = result.getModelAndView();
        Assert.assertNotNull(mav.getModel().get("activity"));
        Activity activity = (Activity) mav.getModel().get("activity");
        Assert.assertEquals((Long)1L, activity.getId());
        Assert.assertEquals("Running", activity.getName());;
    }
    
    @Test
    public void removeActivity() throws Exception {
        MvcResult result = this.mockMvc.perform(get("/activities/remove/{id}", 1))
            .andExpect(status().isFound())
            .andReturn();
        
        ModelAndView mav = result.getModelAndView();
        Assert.assertEquals("redirect:/activities", mav.getViewName());
        
        result = this.mockMvc.perform(get("/activities"))
           .andExpect(status().isOk())
            .andReturn();
            
        mav = result.getModelAndView();
        Assert.assertNotNull(mav.getModel().get("activities"));
        List<Activity> activities = (List<Activity>) mav.getModel().get("activities");
        Assert.assertEquals(1, activities.size());
    }
    
    @Test
    public void saveActivity() throws Exception {
		
        MvcResult result = this.mockMvc.perform(post("/activities")
        		.param("id", "3")
        		.param("name", "Cycling"))
            .andExpect(status().isFound())
            .andReturn();
        
        result = this.mockMvc.perform(get("/activities"))
           .andExpect(status().isOk())
            .andReturn();
            
        ModelAndView mav = result.getModelAndView();
        Assert.assertNotNull(mav.getModel().get("activities"));
        List<Activity> activities = (List<Activity>) mav.getModel().get("activities");
        Assert.assertEquals(3, activities.size());
    }
}
