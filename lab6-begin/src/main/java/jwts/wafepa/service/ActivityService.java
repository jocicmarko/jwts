package jwts.wafepa.service;

import jwts.wafepa.model.Activity;

public interface ActivityService extends CrudService<Activity> {
	
}
