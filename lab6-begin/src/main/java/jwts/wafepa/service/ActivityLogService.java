package jwts.wafepa.service;

import jwts.wafepa.model.ActivityLog;

public interface ActivityLogService extends CrudService<ActivityLog> {

}
