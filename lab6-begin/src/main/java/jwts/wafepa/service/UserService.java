package jwts.wafepa.service;

import jwts.wafepa.model.User;

public interface UserService extends CrudService<User> {

}
