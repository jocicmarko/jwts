package jwts.wafepa.service.impl;

import jwts.wafepa.model.User;
import jwts.wafepa.service.UserService;

import org.springframework.stereotype.Service;

@Service
public class InMemoryUserService extends AbstractInMemoryService<User> implements UserService {

}
