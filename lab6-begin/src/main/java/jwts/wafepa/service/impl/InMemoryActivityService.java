package jwts.wafepa.service.impl;

import jwts.wafepa.model.Activity;
import jwts.wafepa.service.ActivityService;

import org.springframework.stereotype.Service;

@Service
public class InMemoryActivityService extends AbstractInMemoryService<Activity> implements ActivityService {

}
