package jwts.wafepa.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class Activity extends AbstractBaseEntity {

	private static final long serialVersionUID = -4614577547027982134L;
	
	@NotNull
	@Length(min=1, max=30)
	private String name;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
