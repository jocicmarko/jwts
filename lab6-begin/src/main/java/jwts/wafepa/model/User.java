package jwts.wafepa.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

public class User extends AbstractBaseEntity {

	private static final long serialVersionUID = -4567673457577665133L;
	
	@NotEmpty
	@Email
	private String email;
	
	@NotNull
	@Length(min=1, max=30)
	private String firstname;
	
	@NotNull
	@Length(min=1, max=30)
	private String lastname;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
}
