package jwts.wafepa.model;

import java.util.Date;


public class ActivityLog extends AbstractBaseEntity {

	private static final long serialVersionUID = -7951602462350631669L;
	
	private Activity activity;
	private User user;
	private Date date;
	private Integer duration;
	private Double length;
	
	public Activity getActivity() {
		return activity;
	}
	public void setActivity(Activity activity) {
		this.activity = activity;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Integer getDuration() {
		return duration;
	}
	public void setDuration(Integer duration) {
		this.duration = duration;
	}
	public Double getLength() {
		return length;
	}
	public void setLength(Double length) {
		this.length = length;
	}
	
}
