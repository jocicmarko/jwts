package jwts.wafepa.web.controller;

import java.util.List;

import javax.validation.Valid;

import jwts.wafepa.model.ActivityLog;
import jwts.wafepa.model.User;
import jwts.wafepa.service.ActivityLogService;
import jwts.wafepa.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/activityLogs")
public class ActivityLogController {

	@Autowired
	private ActivityLogService activityLogService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String get(Model model) {
		model.addAttribute("activityLogs", activityLogService.findAll());
		return "activityLogs";
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String getNew(Model model) {
		List<User> users = userService.findAll();
		
		model.addAttribute("activityLog", new ActivityLog());
		model.addAttribute("users", users);
		
		return "addEditActivityLog";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String post(@Valid ActivityLog activityLog, BindingResult bindingResult, Model model) {
		String viewName;
		if (!bindingResult.hasErrors()) {
			activityLogService.save(activityLog);
			viewName = "redirect:activityLogs";
		} else {
			model.addAttribute("user", activityLog);
			viewName = "addEditActivityLog";
		}
		
		return viewName;
	}
}
