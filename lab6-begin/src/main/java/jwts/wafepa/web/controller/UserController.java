package jwts.wafepa.web.controller;

import javax.validation.Valid;

import jwts.wafepa.model.User;
import jwts.wafepa.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String get(Model model) {
		model.addAttribute("users", userService.findAll());
		return "users";
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public String getNew(Model model) {
		model.addAttribute("user", new User());
		return "addEditUser";
	}	
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String getEdit(@PathVariable Long id, Model model) {
		model.addAttribute("user", userService.findOne(id));
		return "addEditUser";
	}	
	
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
	public String remove(@PathVariable Long id) {
		userService.remove(id);
		return "redirect:..";
	}	
	
	@RequestMapping(method = RequestMethod.POST)
	public String post(@Valid User user, BindingResult bindingResult, Model model) {
		String viewName;
		if (!bindingResult.hasErrors()) {
			userService.save(user);
			viewName = "redirect:users";
		} else {
			model.addAttribute("user", user);
			viewName = "addEditUser";
		}
		
		return viewName;
	}
}
