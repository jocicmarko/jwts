<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<h2><fmt:message key="page.addEditActivity.header"/></h2>
<div id="addEditActivityLog">
	<c:url var="activityLogs" value="/activityLogs" />
	<a href="${activityLogs}"><fmt:message key="page.addEditActivity.action.back"/></a>
	<form:form id="formActivityLog" action="${activityLogs}" method="post" modelAttribute="activityLog">
		<fieldset>
			<form:hidden path="id" />
			<form:label path="duration"><fmt:message key="common.label.name"/></form:label>
			<form:input path="duration" cssErrorClass="error" />
			<form:errors path="duration" cssClass="errorMessage" />
			<br />
			
			<form:select path="user" cssErrorClass="error">
				<form:options items="${users}" itemLabel="email" />
			</form:select>
			<form:errors path="user" cssClass="errorMessage" />
		</fieldset>
		<div class="highlightcolorBlack">
			<button type="submit" name="save" class="button"><fmt:message key="common.action.save"/></button>
			<button type="reset" name="cancel" class="button"><fmt:message key="common.action.cancel"/></button>
		</div>
	</form:form>
</div>