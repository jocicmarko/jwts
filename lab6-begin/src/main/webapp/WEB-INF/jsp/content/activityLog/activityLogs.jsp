<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<h2><fmt:message key="page.activityLogs.header"/></h2>
<div id="activityLogs">
	<table class="dataTable">
		<tr>
			<th><fmt:message key="common.label.id"/></th>
			<th><fmt:message key="common.label.name"/></th>
			<th></th>
		</tr>
		<c:forEach items="${activityLogs}" var="activityLog">
			<tr>	
				<td><c:out value="${activityLog.id}"/></td>				
				<td><c:out value="${activityLog.duration}"/></td>
				<td><a href="<c:url value="/activityLogs/edit/${activityLog.id}"/>"><fmt:message key="common.action.edit"/></a></td>
				<td><a href="<c:url value="/activityLogs/remove/${activityLog.id}"/>"><fmt:message key="common.action.remove"/></a></td>
			</tr>
		</c:forEach>
	</table>
</div>	
<div id="newActivityLog" class="highlightcolorBlack">
	<a href="<c:url value="/activityLogs/new"/>"><fmt:message key="page.activityLogs.action.addNewActivityLog"/></a>
</div>
