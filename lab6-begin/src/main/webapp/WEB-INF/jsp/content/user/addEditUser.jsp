<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<h2><fmt:message key="page.addEditUser.header"/></h2>
<div id="addEditUser">
	<c:url var="users" value="/users" />
	<a href="${users}"><fmt:message key="page.addEditUser.action.back"/></a>
	<form:form id="formUser" action="${users}" method="post" modelAttribute="user">
		<fieldset>
			<form:hidden path="id" />
			<form:label path="email"><fmt:message key="common.label.email"/> </form:label>
			<form:input path="email" cssErrorClass="error" />
			<form:errors path="email" cssClass="errorMessage" />
			<br />
			
			<form:label path="firstname"><fmt:message key="common.label.firstname"/> </form:label>
			<form:input path="firstname" cssErrorClass="error" />
			<form:errors path="firstname" cssClass="errorMessage" />
			<br />
			
			<form:label path="lastname"><fmt:message key="common.label.lastname"/> </form:label>
			<form:input path="lastname" cssErrorClass="error" />
			<form:errors path="lastname" cssClass="errorMessage" />
		</fieldset>
		<div class="highlightcolorBlack">
			<button type="submit" name="save" class="button"><fmt:message key="common.action.save"/></button>
			<button type="reset" name="cancel" class="button"><fmt:message key="common.action.cancel"/></button>
		</div>
	</form:form>
</div>	