<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<h2><fmt:message key="page.activities.header"/></h2>
<div id="activities">
	<table class="dataTable">
		<tr>
			<th><fmt:message key="common.label.id"/></th>
			<th><fmt:message key="common.label.name"/></th>
			<th></th>
		</tr>
		<c:forEach items="${activities}" var="activity">
			<tr>	
				<td><c:out value="${activity.id}"/></td>				
				<td><c:out value="${activity.name}"/></td>
				<td><a href="<c:url value="/activities/edit/${activity.id}"/>"><fmt:message key="common.action.edit"/></a></td>
				<td><a href="<c:url value="/activities/remove/${activity.id}"/>"><fmt:message key="common.action.remove"/></a></td>
			</tr>
		</c:forEach>
	</table>
</div>	
<div id="newActivity" class="highlightcolorBlack">
	<a href="<c:url value="/activities/new"/>"><fmt:message key="page.activities.action.addNewActivity"/></a>
</div>
