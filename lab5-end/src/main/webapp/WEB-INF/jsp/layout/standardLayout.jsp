<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<tiles:importAttribute name="title" />

<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title><fmt:message key="${title}" /></title>
		
		<link href="<c:url value="/css/screen.css"/>" rel="stylesheet" type="text/css" />
		<link href="http://www.levi9.com/wp-content/themes/levi9/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<script src="<c:url value="/js/lib/jquery/jquery-1.10.2.min.js"/>"></script>
		<script src="<c:url value="/js/main.js"/>"></script>
	</head>
	<body>
		<header>
			<div class="top darkNoise">
				<a href="#" target="_blank"><fmt:message key="common.header.link"/></a>
				<a href="<c:url value="?lang=sr" />"><fmt:message key="common.header.language.sr" /></a>
				<a href="<c:url value="?lang=en" />"><fmt:message key="common.header.language.en" /></a>
			</div>
			<h1><fmt:message key="common.header.title"/></h1>
			<ul id="mainMenu">
				<li><a href="<c:url value="/"/>"><fmt:message key="common.menu.home"/></a></li>
				<li><a href="<c:url value="/activities"/>"><fmt:message key="common.menu.activities"/></a></li>
				<li><a href="<c:url value="/users"/>"><fmt:message key="common.menu.users"/></a></li>
			</ul>
		</header>
		<section id="mainContent">
			<tiles:insertAttribute name="content" />
		</section>
		<footer>
			<div class="darkNoise"><fmt:message key="common.footer.copyright"/></div>
		</footer>
	</body>
</html>