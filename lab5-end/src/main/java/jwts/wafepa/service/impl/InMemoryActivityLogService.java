package jwts.wafepa.service.impl;

import org.springframework.stereotype.Service;

import jwts.wafepa.model.ActivityLog;
import jwts.wafepa.service.ActivityLogService;

@Service
public class InMemoryActivityLogService extends AbstractInMemoryService<ActivityLog> implements ActivityLogService {

}
